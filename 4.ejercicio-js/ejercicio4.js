"use strict";

const names = [
  "A-Jay",
  "Manuel",
  "Manuel",
  "Eddie",
  "A-Jay",
  "Su",
  "Reean",
  "Manuel",
  "A-Jay",
  "Zacharie",
  "Zacharie",
  "Tyra",
  "Rishi",
  "Arun",
  "Kenton",
];

function deleteRepeated(array) {
  const withoutRepeated = [];
  array.map((name) => {
    const repeated = withoutRepeated.find((curr) => name === curr);
    if (!repeated) {
      withoutRepeated.push(name);
    }
  });
  return withoutRepeated;
}

console.log(deleteRepeated(names));
