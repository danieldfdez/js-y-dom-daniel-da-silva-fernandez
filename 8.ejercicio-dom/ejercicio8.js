"use strict";

const body = document.querySelector("body");
const h1 = document.createElement("h1");
const contenedor = document.createElement("div");
const start = document.createElement("button");
const pause = document.createElement("button");
const reset = document.createElement("button");

body.appendChild(h1);
body.appendChild(contenedor);
contenedor.appendChild(start);
contenedor.appendChild(pause);
contenedor.appendChild(reset);

start.textContent = "Start";
pause.textContent = "Pause";
reset.textContent = "Reset";

let hora = 0;
let minuto = 0;
let segundo = 0;

function addDecenas(time) {
  if (time < 10) {
    return (time = `0${time}`);
  }
}

function cronometro() {
  if (segundo > 59) {
    minuto++;
    segundo = 0;
  } else if (minuto > 59) {
    hora++;
    minuto = 0;
  }
  h1.textContent = `${addDecenas(hora)} : ${addDecenas(minuto)} : ${addDecenas(
    segundo
  )}`;
  segundo++;
}

let countThis;
function letsGo() {
  countThis = setInterval(cronometro, 1000);
}

function waitPlease() {
  clearInterval(countThis);
}

function hereWeGoAgain() {
  clearInterval(countThis);
  hora = 0;
  minuto = 0;
  segundo = 0;
  h1.textContent = `${addDecenas(hora)} : ${addDecenas(minuto)} : ${addDecenas(
    segundo
  )}`;
}

cronometro();
start.addEventListener("click", letsGo);
pause.addEventListener("click", waitPlease);
reset.addEventListener("click", hereWeGoAgain);
