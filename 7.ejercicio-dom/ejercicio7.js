"use strict";

const body = document.querySelector("body");
const masEpilepsia = document.createElement("button");
body.appendChild(masEpilepsia);
masEpilepsia.textContent = "Dale mas color!!!";
masEpilepsia.style.marginBottom = "50px";
const drogas = document.createElement("div");
drogas.style.display = "grid";
drogas.style.gridTemplateColumns = "200px 200px 200px 200px 200px 200px";

body.appendChild(drogas);

function unaPastiMas() {
  const pastilla = document.createElement("canvas");
  pastilla.width = "200";
  pastilla.height = "200";
  pastilla.getContext("2d");
  drogas.appendChild(pastilla);
}

function randomColor() {
  const pastiColor = document.getElementsByTagName("canvas");
  console.log(pastiColor);
  for (const colors of pastiColor) {
    let color =
      "rgb(" +
      Math.round(Math.random() * 255) +
      "," +
      Math.round(Math.random() * 255) +
      "," +
      Math.round(Math.random() * 255) +
      ")";
    colors.style.backgroundColor = color;
  }
}

setInterval(randomColor, 1000);

function comeOnBarbieLetsGoParty() {
  let n = +prompt("Cuanto quieres fliparlo?");
  for (let i = 1; i <= n; i++) {
    unaPastiMas();
  }
}
comeOnBarbieLetsGoParty();

masEpilepsia.addEventListener("click", () => {
  unaPastiMas();
});
