"use strict";

const urlUsers = "https://randomuser.me/api/";

async function obtenerUsuarios(url, amount) {
  let users = [];
  for (let i = 0; i < amount; i++) {
    const peticion = await (await fetch(url)).json();
    const datos = peticion.results[0];
    const user = {
      username: datos.login.username,
      nombre: datos.name.first,
      apellido: datos.name.last,
      sexo: datos.gender,
      país: datos.location.country,
      email: datos.email,
      foto: datos.picture.large,
    };
    users.push(user);
  }
  console.log(users);
}

obtenerUsuarios(urlUsers, 6);
