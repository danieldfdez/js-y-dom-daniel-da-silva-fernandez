"use strict";

let seconds = 1;
let minutes = 0;
let hours = 0;
let days = 0;

function clock() {
  ++seconds;
  if (seconds === 60) {
    seconds = 0;
    ++minutes;
  }
  if (minutes === 60) {
    minutes = 0;
    ++hours;
  }
  if (hours === 24) {
    hours = 0;
    ++days;
  }
}
function showClock() {
  console.log(`Day:${days} ${hours}:${minutes}:${seconds}`);
}

setInterval(clock, 1000);
setInterval(showClock, 5000);
