"use strict";

const urlApi = "https://rickandmortyapi.com/api/";

function deleteRepeated(array) {
  const withoutRepeated = [];
  array.map((name) => {
    const repeated = withoutRepeated.find((curr) => name === curr);
    if (!repeated) {
      withoutRepeated.push(name);
    }
  });
  return withoutRepeated;
}

async function januaryCharacters(url) {
  const episodesUrl = (await (await fetch(url)).json()).episodes;
  const episodesList = await (await fetch(episodesUrl)).json();
  const episodes1 = episodesList.results;
  const episodesNext = await (await fetch(episodesList.info.next)).json();
  const episodes2 = episodesNext.results;
  const episodesLast = await (await fetch(episodesNext.info.next)).json();
  const episodes3 = episodesLast.results;
  const episodes = [...episodes1, ...episodes2, ...episodes3];
  const januaryEpisodes = episodes.filter((episode) =>
    episode.air_date.startsWith("January")
  );
  const januaryCharactersUrls = [];
  for (const episode of januaryEpisodes) {
    januaryCharactersUrls.push(...episode.characters);
  }
  const januaryCharactersUrlsWithoutRepeated = deleteRepeated(
    januaryCharactersUrls
  );
  const januaryCharacters = [];
  for (const url of januaryCharactersUrlsWithoutRepeated) {
    januaryCharacters.push((await (await fetch(url)).json()).name);
  }
  console.log(januaryCharacters);
}

januaryCharacters(urlApi);
