"use strict";

let p = document.getElementsByTagName("p");

for (const words of p) {
  const text = words.innerHTML;
  const filter = text.split(" ").filter((word) => word.length > 5);
  words.innerHTML = words.textContent
    .split(" ")
    .map((word) => {
      return filter.indexOf(word) > -1 ? `<span>${word}</span>` : word;
    })
    .join(" ");
}

let span = document.getElementsByTagName("span");

for (const word of span) {
  word.style.textDecoration = "underline";
}
