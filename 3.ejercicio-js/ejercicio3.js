"use strict";

function conversor(numero, base) {
  if (base === 10) {
    return numero.toString(2);
  } else if (base === 2) {
    const arrayNumero = Array.from(`${numero}`).reverse();
    const resultado = arrayNumero.reduce((accumulator, valor, indice) => {
      return (accumulator = accumulator + valor * Math.pow(2, indice));
    }, 0);
    return resultado;
  }
  return "La base no coincide";
}

console.log(conversor(101011110010, 2));
console.log(conversor(2802, 10));
console.log(conversor(72345428, 7));
